package application.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class PersonalData {
	
	private StringProperty firstNameProperty;
	private StringProperty lastNameProperty;
	private StringProperty addressProperty;
	private StringProperty phoneNumberProperty;
	
	public PersonalData() {
		firstNameProperty = new SimpleStringProperty("");
		lastNameProperty = new SimpleStringProperty("");
		addressProperty = new SimpleStringProperty("");
		phoneNumberProperty = new SimpleStringProperty("");
	}
	
	public StringProperty getFirstNameProperty() {
		return firstNameProperty;
	}
	
	public StringProperty getLastNameProperty() {
		return lastNameProperty;
	}

	public StringProperty getAddressProperty() {
		return addressProperty;
	}

	public StringProperty getPhoneNumberProperty() {
		return phoneNumberProperty;
	}

}
