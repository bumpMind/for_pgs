package application;
	
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import application.model.PersonalData;
import application.view.DataOverviewController;
import application.view.EditDataController;
import javafx.application.Application;
import javafx.beans.property.StringProperty;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;

public class MainApplication extends Application {
	
	private final int numberOfScenes = 4;
	
	private Stage stage;
	private List<Scene> lScenes;
	private PersonalData person;

	
 	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception {
		this.stage = stage;
		stage.setTitle("Wizyt�wka");
		
		person = new PersonalData();
		lScenes = new ArrayList<Scene>();
		
		initScenesList(lScenes);
		
		stage.setScene(lScenes.get(0));
		stage.show();
	}
	
	private void initScenesList(List<Scene> lScenes) {
		try {
			lScenes.add(loadEditScene(new String("Imie: "), person.getFirstNameProperty()));
			lScenes.add(loadEditScene(new String("Nazwisko: "), person.getLastNameProperty()));
			lScenes.add(loadEditScene(new String("Adres: "), person.getAddressProperty()));
			lScenes.add(loadEditScene(new String("Telefon: "), person.getPhoneNumberProperty()));
			lScenes.add(loadViewScene());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private Scene loadEditScene(String labelData, StringProperty data) throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(MainApplication.class.getResource("view/EditData.fxml"));	
		
		AnchorPane layout = (AnchorPane)loader.load();
		EditDataController controller = loader.getController();
		
		controller.setLabel(labelData);
		controller.setDataProperty(data);
		controller.setMainApp(this);

		return new Scene(layout);
	}
	
	private Scene loadViewScene() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(MainApplication.class.getResource("view/DataOverview.fxml"));

		AnchorPane layout = (AnchorPane)loader.load();
		DataOverviewController controller = loader.getController();
		
		controller.bindPersonalData(person);
		controller.setMainApp(this);
	
		return new Scene(layout);
	}
	
	
	public void nextScene() {
		int sceneIndex = lScenes.indexOf(stage.getScene());
		
		if( sceneIndex<numberOfScenes ) {
			stage.setScene(lScenes.get(++sceneIndex));
		}
	}
	
	public void prevScene() {
		int sceneIndex = lScenes.indexOf(stage.getScene());
		
		if( sceneIndex>0 ) {
			stage.setScene(lScenes.get(--sceneIndex));
		}
	};
	
}
