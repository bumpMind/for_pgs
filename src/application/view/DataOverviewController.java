package application.view;

import application.MainApplication;
import application.model.PersonalData;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

public class DataOverviewController {

	@FXML
	private Label firstNameProperty;
	
	@FXML
	private Label lastNameProperty;
	
	@FXML
	private Label addressProperty;
	
	@FXML
	private Label phoneNumberProperty;
	
	
	private MainApplication mainStage;
	
	
	@FXML
	private void initialize() {}
	
	@FXML
	private void handleButtonPrevScene() {
		mainStage.prevScene();
	}
	
	@FXML
	private void handleButtonClose() {
		Platform.exit();
	}
	
	
	public void bindPersonalData(PersonalData person) {
		firstNameProperty.textProperty().bind(person.getFirstNameProperty());
		lastNameProperty.textProperty().bind(person.getLastNameProperty());
		addressProperty.textProperty().bind(person.getAddressProperty());
		phoneNumberProperty.textProperty().bind(person.getPhoneNumberProperty());
	}
	
	public void setMainApp(MainApplication mainStage) {
		this.mainStage = mainStage;
	}
	
}
