package application.view;

import application.MainApplication;
import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class EditDataController {
	
	@FXML
	private Label dataLabel;
	
	@FXML
	private TextField dataTextField;
	
	
	private MainApplication mainStage;
	
	
	
	@FXML
	private void initialize() {}
	
	@FXML
	private void handleButtonNextScene() {
		mainStage.nextScene();
	}
	
	@FXML
	private void handleButtonPrevScene() {
		mainStage.prevScene();
	}
	
	
	public void setLabel(String textLabel) {
		dataLabel.setText(textLabel);
	}
	
	public void setDataProperty(StringProperty dataProperty) {
		dataTextField.textProperty().bindBidirectional(dataProperty);
	}
	
	public void setMainApp(MainApplication mainStage) {
		this.mainStage = mainStage;
	}
	
}
